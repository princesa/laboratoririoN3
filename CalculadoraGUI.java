import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CalculadoraGUI extends JFrame {
    private JTextField textField;
    private JButton button1, button2, button3, button4, button5, button6, button7, button8, button9, button0;
    private JButton buttonSuma, buttonResta, buttonMultiplicacion, buttonDivision, buttonIgual, buttonLimpiar;
    private String operacion;

    public CalculadoraGUI() {
        super("Calculadora");
        setLayout(new BorderLayout());
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(300, 400);
        setLocationRelativeTo(null);
        textField = new JTextField();
        add(textField, BorderLayout.NORTH);
        JPanel panelNumeros = new JPanel(new GridLayout(4, 3));
        button1 = new JButton("1");
        button2 = new JButton("2");
        button3 = new JButton("3");
        button4 = new JButton("4");
        button5 = new JButton("5");
        button6 = new JButton("6");
        button7 = new JButton("7");
        button8 = new JButton("8");
        button9 = new JButton("9");
        button0 = new JButton("0");
        panelNumeros.add(button1);
        panelNumeros.add(button2);
        panelNumeros.add(button3);
        panelNumeros.add(button4);
        panelNumeros.add(button5);
        panelNumeros.add(button6);
        panelNumeros.add(button7);
        panelNumeros.add(button8);
        panelNumeros.add(button9);
        panelNumeros.add(button0);
        add(panelNumeros, BorderLayout.CENTER);
        JPanel panelOperaciones = new JPanel(new GridLayout(5, 1));
        buttonSuma = new JButton("+");
        buttonResta = new JButton("-");
        buttonMultiplicacion = new JButton("*");
        buttonDivision = new JButton("/");
        buttonIgual = new JButton("=");
        panelOperaciones.add(buttonSuma);
        panelOperaciones.add(buttonResta);
        panelOperaciones.add(buttonMultiplicacion);
        panelOperaciones.add(buttonDivision);
        panelOperaciones.add(buttonIgual);
        add(panelOperaciones, BorderLayout.EAST);
        buttonLimpiar = new JButton("C");
        add(buttonLimpiar, BorderLayout.SOUTH);
        button1.addActionListener(new NumeroListener());
        button2.addActionListener(new NumeroListener());
        button3.addActionListener(new NumeroListener());
        button4.addActionListener(new NumeroListener());
        button5.addActionListener(new NumeroListener());
        button6.addActionListener(new NumeroListener());
        button7.addActionListener(new NumeroListener());
        button8.addActionListener(new NumeroListener());
        button9.addActionListener(new NumeroListener());
        button0.addActionListener(new NumeroListener());
        buttonSuma.addActionListener(new OperacionListener());
        buttonResta.addActionListener(new OperacionListener());
        buttonMultiplicacion.addActionListener(new OperacionListener());
        buttonDivision.addActionListener(new OperacionListener());
        buttonIgual.addActionListener(new ResultadoListener());
        buttonLimpiar.addActionListener(new LimpiarListener());

        operacion = "";
    }

    class NumeroListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String digit = ((JButton) e.getSource()).getText();
            operacion += digit;
            textField.setText(operacion);
        }
    }

    class OperacionListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String operador = ((JButton) e.getSource()).getText();
            operacion += " " + operador + " ";
            textField.setText(operacion);
        }
    }

    class ResultadoListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String[] elementos = operacion.split(" ");
            double resultado = Double.parseDouble(elementos[0]);

            for (int i = 1; i < elementos.length; i += 2) {
                String operador = elementos[i];
                double numero = Double.parseDouble(elementos[i + 1]);

                if (operador.equals("+")) {
                    resultado += numero;
                } else if (operador.equals("-")) {
                    resultado -= numero;
                } else if (operador.equals("*")) {
                    resultado *= numero;
                } else if (operador.equals("/")) {
                    resultado /= numero;
                }
            }

            textField.setText(Double.toString(resultado));
            operacion = "";
        }
    }

    class LimpiarListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            textField.setText("");
            operacion = "";
        }
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new CalculadoraGUI().setVisible(true);
            }
        });
    }
}